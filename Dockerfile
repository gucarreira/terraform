FROM hashicorp/terraform:light

LABEL nome="terraform"

WORKDIR /app_terraform

ENTRYPOINT [ "" ]

CMD "sh"