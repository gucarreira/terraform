#! /bin/bash
sudo su
echo "br_netfilter ip_vs_rr ip_vs_wrr ip_vs_sh nf_conntrack_ipv4 ip_vs" > /etc/modules-load.d/k8s.conf
    
curl -fsSL https://get.docker.com | bash

apt-get update -y && apt-get install -y apt-transport-https

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
    
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/k8s.list
            
apt-get update  && apt-get install -y kubelet kubeadm  kubectl

kubeadm config images pull